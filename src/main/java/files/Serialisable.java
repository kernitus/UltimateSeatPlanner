package files;

public interface Serialisable {

	void save(USPFile file);
	void load(USPFile file);
}
