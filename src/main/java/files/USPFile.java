package files;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class USPFile extends File {

	//This is a semicolon-separated file

	private Map<String, String> map = new HashMap<>();

	public USPFile(String path) {
		super(System.getProperty("user.home") +
				File.separator + "UltimateSeatPlanner" +
				File.separator + path + ".usp");

		//Load the whole file up into a map
		if(isDirectory() || !exists()) return;

		try {
			Scanner scan = new Scanner(this);

			String currentLine = "";

			while(scan.hasNextLine()){
				currentLine = scan.nextLine();
				String[] values = currentLine.split(";");
				map.put(values[0], values[1]);
			}

			scan.close();

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Write map to file
	 */
	public void save(){
		try {
			getParentFile().mkdirs();
			createNewFile();
			FileWriter writer = new FileWriter(this);

			for(String key : map.keySet())
				writer.write(key + ";" + map.get(key) + "\n");

			writer.close();

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public Map<String, String> getData(){
		return map;
	}
}