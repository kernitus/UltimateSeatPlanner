package data;

/**
 * Stores location of objects in classroom
 */
public class Location {

	private double x, y;

	public Location(String serialisedLocation){
		this(Double.valueOf(serialisedLocation.split("_")[0]),
				Double.valueOf(serialisedLocation.split("_")[1]));
	}

	public Location(double x, double y){
		setX(x);
		setY(y);
	}
	
	//Getters
	public double getX(){
		return x;
	}
	public double getY(){
		return y;
	}
	
	//Setters
	public void setX(double x){
		this.x = x;
	}
	public void setY(double y){
		this.y = y;
	}

	@Override
	public String toString(){
		return x + "_" + y;
	}
}
