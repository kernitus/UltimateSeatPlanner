package data;

import files.USPFile;
import utils.StudentLabel;

import java.io.File;
import java.util.ArrayList;
import java.util.Map;

public class Plan extends Data {

    private final StudentClass studentClass;
    private final Room room;
    private final ArrayList<StudentLabel> studentLabels = new ArrayList<>();

    //Plan holds a room and a class but there can be more than one plan for this combination
    //So the name must be unique, decided by the user

    public Plan(Room room, StudentClass studentClass, String name){
        this.studentClass = studentClass;
        this.room = room;
        this.name = name;
    }

    //Getters

    public StudentClass getStudentClass() {
        return studentClass;
    }
    public Room getRoom() {
        return room;
    }
    public int getStudentCount(){
        return studentClass.getStudentCount();
    }
    public int getSeatCount(){
        return room.getSeatCount();
    }
    public int getTableCount(){
        return room.getTableCount();
    }

    @Override
    public void save() {
        //save location of StudentLabels
        USPFile file = new USPFile(getLocalFolderPath());
        Map<String,String> data = file.getData();
        data.clear();
        studentLabels.forEach(student -> {
            if(student.isInUse())//Don't save unused labels
                data.put(student.getStudent().getFullName(),
                        student.getLayoutX() + "_" + student.getLayoutY());
        });
        file.save();
    }

    @Override
    public void load() {
        //Load student labels and set correct position
        USPFile file = new USPFile(getLocalFolderPath());
        Map<String,String> data = file.getData();

        //For each student in class make new StudentLabel
        studentLabels.forEach(label -> {
            //Get location from plan file map
            try {//Try catch to exclude students that weren't saved in file
                String[] location = data.get(label.getStudent().getFullName()).split("_");
                label.setInUse(true);
                label.setLayoutX(Double.parseDouble(location[0]));
                label.setLayoutY(Double.parseDouble(location[1]));
            } catch (Exception ignored) {}
        });
    }

    @Override
    public String getLocalFolderPath() {
        //Plan file name: roomName_className_planName.usp
        return "plans" + File.separator +
                room.getName() + "_" + studentClass.getName()
                + "_" + name;
    }

    public void addStudentLabel(StudentLabel studentLabel){
        studentLabels.add(studentLabel);
    }
    public void removeStudentLabel(StudentLabel studentLabel){
        studentLabels.remove(studentLabel);
    }
    public ArrayList<StudentLabel> getStudentLabels(){
        return studentLabels;
    }
}
