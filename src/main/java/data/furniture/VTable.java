package data.furniture;

import data.Location;

public class VTable extends RectangleTable {

    public VTable(Location loc){
        super(80, 100, 20, 20, loc);
    }

    public VTable(){
        this(null);
    }
}
