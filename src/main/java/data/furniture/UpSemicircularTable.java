package data.furniture;

import data.Location;

public class UpSemicircularTable extends Table {

    public UpSemicircularTable(Location loc){
        super(70, 70, 70, 70, loc);
    }

    public UpSemicircularTable(){
        this(null);
    }

    @Override
    protected void addSeats() {

    }

    @Override
    public void removeSeats() {

    }

    @Override
    public void setupSeats(boolean setupClickToggle) {
        //todo fill in
    }
}
