package data.furniture;

import data.Location;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Extends rectangle so it may directly be placed on grid
 * @implNote Serialisable for saving to file
 */
public abstract class Table extends Rectangle implements Serializable {

    /*For each grid square of the table there is an int as seat position,
    starting from the bottom left corner, clockwise*/
    protected final List<Seat> seats = new ArrayList<>();

    public Table(double width, double height, double arcWidth, double arcHeight, Location loc){
        super(width, height, Color.SADDLEBROWN);
        setArcWidth(arcWidth);
        setArcHeight(arcHeight);
        if(loc != null) setLocation(loc);
    }

    //Getters
    public Location getLocation(){
        return new Location(getLayoutX(), getLayoutY());
    }
    public int getSeatCount(){
        return seats.size();
    }
    public List<Seat> getSeats(){
        return seats;
    }
    public String getSeatsSerialised(){
        String s = "";
        for (Seat seat : seats)
            s += seat.toString() + "_";
        return s;
    }

    //Setters
    public void setLocation(Location loc){
        setLocation(loc.getX(), loc.getY());
    }
    public void setLocation(double x, double y){
        setLayoutX(x);
        setLayoutY(y);
    }

    //Serialisation
    public String save(){
        return getWidth() + "_" + getHeight() + "_" + getArcWidth() + "_" + getArcHeight() + "_" + getLocation().toString();
    }

    protected abstract void addSeats();
    public abstract void removeSeats();
    public abstract void setupSeats(boolean setupClickToggle);
}
