package data.furniture;

import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;

import java.io.Serializable;

/**
 * Seat around a table:
 * can be clicked on to de/activate
 * Is deactivated by default
 */
public class Seat extends Rectangle implements Serializable {

	/**Position around the table*/
	private int position;
	private boolean active;
	
	public Seat(int position) {
		super(20, 20, Color.RED);
		this.position = position;
		setStyle("-fx-stroke: black; -fx-stroke-width: 2");
	}
	
	//Getters
	public int getPosition(){
		return position;
	}
	@Override
	public String toString(){
		return String.valueOf(active);
	}
	
	//Setters
	public void setPosition(int position){
		this.position = position;
	}

	public void setActive(boolean active){
	    this.active = active;
	    updateActive();
    }
    public boolean isActive(){
		return active;
	}

    /**
     * Update seat colour depending if active
     */
    private void updateActive(){
        if(active) setFill(Color.GREEN);
        else setFill(Color.RED);
    }

	/**
	 * Allows (de)activation by clicking
	 */
	public void setupOnClick(){
		setOnMouseClicked(event -> {
            active = !active;
		    updateActive();
        });
	}
}
