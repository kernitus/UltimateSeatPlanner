package data.furniture;

import data.Location;
import javafx.scene.Node;
import javafx.scene.layout.Pane;

import java.util.ArrayList;
import java.util.List;

public class HTable extends RectangleTable {

    public HTable(Location loc){
        super(100, 80, 20, 20, loc);
    }

    public HTable(){
        this(null);
    }
}
