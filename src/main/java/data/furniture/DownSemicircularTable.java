package data.furniture;

import data.Location;

public class DownSemicircularTable extends Table {

    public DownSemicircularTable(Location loc){
        super(70, 70, 70, 70, loc);
    }

    public DownSemicircularTable(){
        this(null);
    }

    @Override
    protected void addSeats() {

    }

    @Override
    public void removeSeats() {

    }

    @Override
    public void setupSeats(boolean setupClickToggle) {
        //todo fill in
    }
}
