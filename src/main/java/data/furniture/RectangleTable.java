package data.furniture;

import data.Location;
import javafx.collections.ObservableList;
import javafx.scene.Node;
import javafx.scene.layout.Pane;

public abstract class RectangleTable extends Table {

    public RectangleTable(double width, double height,
                          double arcWidth,
                          double arcHeight,
                          Location loc) {
        super(width,height,arcWidth,arcHeight,loc);
        addSeats();
    }

    protected void addSeats() {
        Seat seat0 = new Seat(0);
        Seat seat1 = new Seat(1);
        Seat seat2 = new Seat(2);
        Seat seat3 = new Seat(3);
        Seat seat4 = new Seat(4);
        Seat seat5 = new Seat(5);

        seats.add(seat0);
        seats.add(seat1);
        seats.add(seat2);
        seats.add(seat3);
        seats.add(seat4);
        seats.add(seat5);

        double width = getWidth();
        double height = getHeight();
        double size = seat0.getWidth();

        double x = getX();
        double y = getY();

        seat0.setX(x + width / 4 - size / 2);
        seat0.setY(y);

        seat1.setX(x + width / 4 * 3 - size / 2);
        seat1.setY(y);

        seat2.setX(x + width - size);
        seat2.setY(y + height / 2 - size / 2);

        seat3.setX(x + width / 4 * 3 - size / 2);
        seat3.setY(y + height - size);

        seat4.setX(x + width / 4 - size / 2);
        seat4.setY(y + height - size);

        seat5.setX(x);
        seat5.setY(y + height / 2 - size / 2);
    }

    public void removeSeats(){
        ObservableList<Node> children = ((Pane) getParent()).getChildren();
        seats.forEach(children::remove);
    }

    public void setupSeats(boolean setupClickToggle){
        ObservableList<Node> children = ((Pane) getParent()).getChildren();

        seats.forEach(seat -> {
            if(setupClickToggle){
                seat.setupOnClick();
                children.add(seat);
            }
            else if (seat.isActive())
                children.add(seat);

            seat.layoutXProperty().bind(layoutXProperty());
            seat.layoutYProperty().bind(layoutYProperty());
            seat.toFront();
        });
    }
}
