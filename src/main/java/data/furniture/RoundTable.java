package data.furniture;

import data.Location;

public class RoundTable extends Table {

    public RoundTable(Location loc){
        super(70, 70, 70, 70, loc);
    }

    public RoundTable(){
        this(null);
    }

    @Override
    protected void addSeats() {

    }

    @Override
    public void removeSeats() {

    }

    @Override
    public void setupSeats(boolean setupClickToggle) {
        //todo fill in
    }
}
