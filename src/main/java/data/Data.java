package data;

import files.USPFile;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.Calendar;
import java.util.Date;

public abstract class Data {

	protected String name;

	//Getters
	public String getName(){
		return name;
	}
	@Override
	public String toString(){
		return name;
	}
	public String getLastEdited(){
		try {
			Date date = new Date(Files.getLastModifiedTime(getFolder().toPath()).toInstant().getEpochSecond() * 1000);
			Calendar cal = Calendar.getInstance();
			cal.setTime(date);

			return cal.get(Calendar.DATE) + "/" + cal.get(Calendar.MONTH) + "/" + cal.get(Calendar.YEAR) + " " + cal.get(Calendar.HOUR_OF_DAY) + ":" + cal.get(Calendar.MINUTE);
		} catch (IOException ignore) {} //Usually because data file hasn't been saved yet
		return null;
	}

	public File getFolder(){
		return new File(getFullFolderPath() + ".usp");
	}

	public static String getUSPDirectoryPath(){
	    return System.getProperty("user.home") + File.separator + "UltimateSeatPlanner" + File.separator;
    }
	public String getFullFolderPath(){
	    return getUSPDirectoryPath() + getLocalFolderPath();
    }
	public abstract String getLocalFolderPath();


	public abstract void save();

	public abstract void load();

	public void setName(String name){
		File current = getFolder();
		this.name = name;
		current.renameTo(getFolder());
	}

    public void delete(){
        new USPFile(getLocalFolderPath()).delete();
    }

	public void rename(String newName){
		if(newName.length() < 1) return;
		File oldFile = new File(getFullFolderPath() + ".usp");
		name = newName;
		oldFile.renameTo(new File(getFullFolderPath() + ".usp"));
	}
}
