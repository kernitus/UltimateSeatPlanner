package data;

import data.furniture.Seat;
import data.furniture.Table;
import files.USPFile;

import java.io.File;
import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class Room extends Data {

	private final List<Table> tables = new ArrayList<>();

	/**
	 * Creates room from name and tries to load from file if exists
	 * @param name Room name
	 */
	public Room(String name){
		this.name = name;
		load();
	}

	//Getters
	public int getSeatCount(){
		int count = 0;
		for(Table table : tables)
			count += table.getSeatCount();
		return count;
	}
	public int getTableCount(){
		return tables.size();
	}
	public List<Table> getTables(){
		return tables;
	}

	public void addTable(Table table){
		tables.add(table);
	}
	public void removeTable(Table table){
		table.removeSeats();
		tables.remove(table);
	}

	@Override
	public void save() {
		USPFile file = new USPFile(getLocalFolderPath());
		Map<String, String> data = file.getData();
		data.clear();
		//Use location as key and table class as value
		tables.forEach(table -> data.put(table.getLocation().toString(),
				table.getClass().getName() + "_" + table.getSeatsSerialised()));
		file.save();
	}

	/**
	 * Gets called automatically upon new object creation
	 */
	@Override
	public void load() {
		USPFile file = new USPFile(getLocalFolderPath());
		if(!file.exists()) return;

		Map<String, String> data = file.getData();

		data.keySet().forEach(key -> {
			String[] values = data.get(key).split("_");
			try {
				Location location = new Location(key);

				Class<?> clazz = Class.forName(values[0]);
				Constructor<?> ctor = clazz.getConstructor(Location.class);
				Table table = (Table) ctor.newInstance(location);
				tables.add(table);

				List<Seat> seats = table.getSeats();
				int i = 0;
				while(i < seats.size()){
					seats.get(i).setActive(Boolean.valueOf(values[i+1]));
					i++;
				}
			}
			catch (Exception e){
				e.printStackTrace();
			}
		});
	}

	@Override
	public String getLocalFolderPath() {
		return "rooms" + File.separator + name;
	}

	public boolean exists(){
		return new USPFile(getLocalFolderPath()).exists();
	}
}
