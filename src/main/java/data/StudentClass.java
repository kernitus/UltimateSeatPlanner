package data;

import files.USPFile;

import java.io.File;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class StudentClass extends Data implements Serializable {

    private List<Student> students = new ArrayList<>();

    public StudentClass(String name){
        this.name = name;
    }

    public void addStudent(Student student){
        students.add(student);
    }
    public void removeStudent(Student student){
        students.remove(student);
        new USPFile(getLocalFolderPath() + student.getFullName()).delete();
    }
    public List<Student> getStudents(){
        return students;
    }
    public int getStudentCount() {
        File folder = new File(getFullFolderPath());
        File[] files = folder.listFiles();
        if(files==null) return 0;
        return files.length;
    }

    @Override
    public String getLocalFolderPath() {
        return "classes" + File.separator + name + File.separator;
    }

    @Override
    public File getFolder(){
        return new File(getFullFolderPath());
    }

    /**
     * Delete class and student files
     */
    @Override
    public void delete(){
        deleteStudentFiles();
        new File(getFullFolderPath()).delete();
    }
    /**
     * Delete all the student files in class folder
     */
    private void deleteStudentFiles(){
        //Loop through all student files and delete them
        File folder = new File(getFullFolderPath());
        File[] files = folder.listFiles();
        if(files != null) {
            for (File file : files)
                file.delete();
        }
    }

    /**
     * Save all student files to class folder
     */
    @Override
    public void save() {
        deleteStudentFiles();//Remove these in case student name changed
        students.forEach(student -> student.save(new USPFile("classes" + File.separator + name + File.separator + student.getFullName())));
    }

    /**
     * Load all student files from folder
     */
    @Override
    public void load() {
        students.clear();
        File folder = new File(getFullFolderPath());
        File[] files = folder.listFiles();
        if(files==null) return;
        for (File file : files) {
            USPFile currentFile = new USPFile(getLocalFolderPath() + file.getName().replaceAll(".usp", ""));
            Student student = new Student(currentFile);
            students.add(student);
        }
    }

    @Override
    public void rename(String newName){
        if(newName.length() < 1) return;
        newName = newName.toUpperCase();
        File oldFile = new File(getFullFolderPath());
        name = newName;
        oldFile.renameTo(new File(getFullFolderPath()));
    }
}
