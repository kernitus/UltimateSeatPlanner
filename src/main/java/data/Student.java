package data;

import files.Serialisable;
import files.USPFile;

import java.util.Map;

public class Student implements Serialisable {

	public Student(boolean male, boolean EAL, boolean SEN, boolean GT, String firstName, String surname){
		this.male = male;
		this.EAL = EAL;
		this.SEN = SEN;
		this.firstName = firstName;
		this.surname = surname;
		this.GT = GT;
	}

	public Student(USPFile file) {
		load(file);
	}

	private Boolean male, EAL, SEN, GT;
	private String firstName, surname;
	
	//Getters
	public boolean isMale(){
		return male;
	}
	public boolean isEAL(){
		return EAL;
	}
	public boolean isSEN(){
		return SEN;
	}
	public boolean isGT(){
		return GT;
	}
	public String getFirstName(){
		return firstName;
	}
	public String getSurname(){
		return surname;
	}
	public String getFullName(){
	    return firstName + "_" + surname;
	}
    public String getHumanReadableFullName(){
        return firstName + " " + surname;
    }

	//Setters
    public void setMale	(boolean male){
        this.male = male;
    }
    public void setSex(String sex){
	    if(sex.equalsIgnoreCase("male"))
	        this.male = true;
	    else this.male = false;
    }
    public void setEAL(boolean EAL){
        this.EAL = EAL;
    }
	public void setSEN(boolean SEN){
	    this.SEN = SEN;
    }
    public void setGT(boolean GT){
        this.GT = GT;
    }public void setEAL(String yesno){
		EAL = yesnotobool(yesno);
	}
	public void setSEN(String yesno){
		SEN = yesnotobool(yesno);;
	}
	public void setGT(String yesno){
		GT = yesnotobool(yesno);;
	}
    public void setFirstName(String firstName){
        this.firstName = firstName;
    }
    public void setSurname(String surname){
        this.surname = surname;
    }

	//Human-readable variable
	public String getSex(){
		if(male) return "Male";
		else return "Female";
	}
	private String getyesno(boolean bool){
	    if(bool) return "Yes";
	    else return "No";
    }
    private boolean yesnotobool(String yesno){
		if (yesno.equalsIgnoreCase("yes")) return true;
		else return false;
	}
	public String getEAL(){
	    return getyesno(EAL);
    }
    public String getSEN(){
        return getyesno(SEN);
    }
    public String getGT(){
        return getyesno(GT);
    }

	@Override
	public void save(USPFile file) {
		Map<String, String> map = file.getData();
		map.put("male", male.toString());
		map.put("EAL", EAL.toString());
		map.put("SEN", SEN.toString());
		map.put("GT", GT.toString());
		map.put("firstName", firstName);
		map.put("surname", surname);

		file.save();
	}

	@Override
	public void load(USPFile file) {
		Map<String, String> map = file.getData();
		male = Boolean.valueOf(map.get("male"));
		EAL = Boolean.valueOf(map.get("EAL"));
		SEN = Boolean.valueOf(map.get("SEN"));
		GT = Boolean.valueOf(map.get("GT"));
		firstName = String.valueOf(map.get("firstName"));
		surname = String.valueOf(map.get("surname"));
	}
}
