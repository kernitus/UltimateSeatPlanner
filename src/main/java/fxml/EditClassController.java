package fxml;

import data.Student;
import data.StudentClass;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.ComboBoxTableCell;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.scene.input.MouseEvent;
import javafx.util.converter.DefaultStringConverter;
import utils.Utils;

import java.net.URL;
import java.util.ResourceBundle;

public class EditClassController extends AbstractController implements Initializable {

    @FXML private TextField nameField;
    @FXML private TableView<Student> studentsTable;
    @FXML private TableColumn<Student, String> firstName;
    @FXML private TableColumn<Student, String> surname;
    @FXML private TableColumn<Student, String> sex;
    @FXML private TableColumn<Student, String> EAL;
    @FXML private TableColumn<Student, String> SEN;
    @FXML private TableColumn<Student, String> GT;

    private ObservableList<Student> students = FXCollections.observableArrayList();
    private StudentClass studentClass;
    private static EditClassController INSTANCE;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        INSTANCE = this;
        setupTable();
    }

    public static EditClassController getINSTANCE() {
        return INSTANCE;
    }

    public void load(StudentClass studentClass){
        this.studentClass = studentClass;
        load();
    }
    private void load(){
        //Fill in name textbox
        nameField.setText(studentClass.getName());

        new Thread(() -> {
            studentClass.load();
            students.addAll(studentClass.getStudents());
        }).start();
    }

    //Button handlers
    public void handleBackClick(MouseEvent event){
        handleSaveClick();
        super.handleBackClick(event);
    }
    public void handleHomeClick(MouseEvent event){
        handleSaveClick();
        super.handleHomeClick(event);
    }
    public void handleAddStudentClick(){
        Student newStudent = new Student(true,false,false,false,"NAME","SURNAME");
        students.add(newStudent);
        studentClass.addStudent(newStudent);
    }
    public void handleSaveClick(){
        String newName = nameField.getText();
        if(!newName.equals(studentClass.getName())){
            studentClass.rename(newName);
        }
        studentClass.save();
    }
    public void handleRemoveStudentClick(){
        Student student = studentsTable.getSelectionModel().getSelectedItem();
        students.remove(student); //Remove from table
        studentClass.removeStudent(student); //Remove from class
    }

    /**
     * Delete entire class
     */
    public void handleDeleteClick(MouseEvent event){
        if(!Utils.deleteAlert()) return;
        studentClass.delete();
        Utils.goBack(this, ((Node) event.getSource()));
    }

    /**
     *Allow table values editing
     */
    private void setupTable(){
        firstName.setCellValueFactory(new PropertyValueFactory<>("firstName"));
        surname.setCellValueFactory(new PropertyValueFactory<>("surname"));
        sex.setCellValueFactory(new PropertyValueFactory<>("sex"));
        EAL.setCellValueFactory(new PropertyValueFactory<>("EAL"));
        SEN.setCellValueFactory(new PropertyValueFactory<>("SEN"));
        GT.setCellValueFactory(new PropertyValueFactory<>("GT"));

        firstName.setCellFactory(TextFieldTableCell.forTableColumn());
        surname.setCellFactory(TextFieldTableCell.forTableColumn());

        ObservableList<String> sexes = FXCollections.observableArrayList("Male","Female");
        sex.setCellFactory(ComboBoxTableCell.forTableColumn(new DefaultStringConverter(), sexes));

        ObservableList<String> yesno = FXCollections.observableArrayList("Yes","No");
        EAL.setCellFactory(ComboBoxTableCell.forTableColumn(new DefaultStringConverter(), yesno));
        SEN.setCellFactory(ComboBoxTableCell.forTableColumn(new DefaultStringConverter(), yesno));
        GT.setCellFactory(ComboBoxTableCell.forTableColumn(new DefaultStringConverter(), yesno));

        studentsTable.setItems(students);
    }

    public void onCommit(TableColumn.CellEditEvent event){

        String newValue = String.valueOf(event.getNewValue());
        Student student = (Student) event.getRowValue();

        switch(event.getTableColumn().getId()){
            case "firstName":
                student.setFirstName(newValue);
                break;
            case "surname":
                student.setSurname(newValue);
                break;
            case "sex":
                student.setSex(newValue);
                break;
            case "EAL":
                student.setEAL(newValue);
                break;
            case "SEN":
                student.setSEN(newValue);
                break;
            case "GT":
                student.setGT(newValue);
                break;
        }
    }

}
