package fxml;

import data.StudentClass;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TablePosition;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import utils.Utils;

import java.io.File;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;

public class ClassesController extends AbstractController implements Initializable {

    private final ObservableList<StudentClass> data = FXCollections.observableArrayList();

    @FXML private TableView<StudentClass> classesTable;
    @FXML private TableColumn<StudentClass, String> name;
    @FXML private TableColumn<StudentClass, String> studentCount;
    @FXML private TableColumn<StudentClass, String> lastEdited;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        setupTable();
    }

    private void setupTable(){
        name.setCellValueFactory(new PropertyValueFactory<>("name"));
        studentCount.setCellValueFactory(new PropertyValueFactory<>("studentCount"));
        lastEdited.setCellValueFactory(new PropertyValueFactory<>("lastEdited"));

        //populate the table asynchronously to reduce lag
        new Thread(this::populateTable).start();
    }

    /**
     * Add a new class to the table
     */
    public void handleNewClassClick(MouseEvent event) throws Exception {
        String newName = Utils.inputDialog("Please enter class name:");
        data.add(new StudentClass(newName));
    }

    /**
     * Handle clicking on table cell
     */
    public void handleTableClick(){
        //Single click should open corresponding room
        TablePosition fcell = classesTable.getFocusModel().getFocusedCell();
        //Get class from clicked cell
        int row = fcell.getRow();
        StudentClass sc = data.get(row);

        Utils.goForward(this,classesTable,"EditClass.fxml");
        EditClassController.getINSTANCE().load(sc);
    }

    private void populateTable(){
        classesTable.setItems(data);
        data.addAll(getClasses());
    }

    public static ArrayList<StudentClass> getClasses(){
        ArrayList<StudentClass> classes = new ArrayList<>();
        File[] files = new File(StudentClass.getUSPDirectoryPath() + "classes" + File.separator).listFiles();
        if((files != null ? files.length : 0) <= 0) return classes;
        for (File file : files) {
            StudentClass sclass = new StudentClass(file.getName().replaceAll(".usp", ""));
            classes.add(sclass);
        }
        return classes;
    }
}
