package fxml;

import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import utils.Utils;

import java.net.URL;
import java.util.ResourceBundle;

public class MainController extends BorderPane implements Initializable {

    public void handleRoomsClick(MouseEvent event){
        Utils.goForward(this,((Node) event.getSource()),"/fxml/RoomsMenu.fxml");
    }
    public void handleClassesClick(MouseEvent event){
        Utils.goForward(this, ((Node) event.getSource()),"/fxml/ClassesMenu.fxml");
    }
    public void handlePlansClick(MouseEvent event){
        Utils.goForward(this,((Node) event.getSource()),"/fxml/PlansMenu.fxml");
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        Utils.sceneHistoryGoForward("/fxml/MainMenu.fxml");
    }
}
