package fxml;

import data.Plan;
import data.Room;
import data.StudentClass;
import data.furniture.Table;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Cursor;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.Pane;
import javafx.scene.text.TextAlignment;
import utils.StudentLabel;
import utils.Utils;

import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;

public class EditPlanController extends AbstractController implements Initializable {

    @FXML private Pane grid;
    @FXML private ImageView gridImage;
    @FXML private FlowPane studentsPane;
    @FXML private TextField name;
    @FXML private Label roomLabel;
    @FXML private Label classLabel;

    private Room room;
    private StudentClass studentClass;
    private Plan plan;

    private static EditPlanController INSTANCE;

    public static EditPlanController getInstance() {
        return INSTANCE;
    }

    public void handleBackClick(MouseEvent event){
        save();//Save on back just in case
        super.handleBackClick(event);
    }
    public void handleHomeClick(MouseEvent event){
        save();
        super.handleHomeClick(event);
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        setupGrid();
        INSTANCE = this;
    }

    /**
     * Setup grid image to scale with parent grid Pane
     */
    private void setupGrid(){
        gridImage.setFitWidth(grid.getWidth());
        gridImage.setFitHeight(grid.getHeight());
        gridImage.fitWidthProperty().bind(grid.widthProperty());
        gridImage.fitHeightProperty().bind(grid.heightProperty());
    }

    /**
     * Set mouse cursor to hand
     */
    private void setCursorToHand(MouseEvent event){
        ((Node) event.getTarget()).setCursor(Cursor.HAND);
    }

    private void setupNewTable(Table table){
        grid.getChildren().add(table);
        table.setupSeats(false);
    }

    /**
     * Save the plan to file
     */
    public void save(){
        String newName = name.getText();
        if(!newName.equals(plan.getName())){//User changed plan name
            plan.rename(newName);
        }
        plan.save();
    }
    /**
     * Deletes room
     */
    public void handleDeleteClick(MouseEvent event){
        if(!Utils.deleteAlert()) return;
        plan.delete();
        Utils.goBack(this, ((Node) event.getSource()));
    }
    /**
     * Load the plan from file
     */
    public void load(Plan plan){
        this.plan = plan;
        this.room = plan.getRoom();
        this.studentClass = plan.getStudentClass();
        load();
    }
    private void load(){
        room.load();
        studentClass.load();

        List<Table> tables = room.getTables();
        Table[] tablesArray = tables.toArray(new Table[tables.size()]);
        tables.clear();
        for (Table table : tablesArray)
            setupNewTable(table);

        //Fill in name textbox
        name.setText(plan.getName());

        //Set room and class labels
        roomLabel.setText("Room: " + room.getName());
        classLabel.setText("Class: " + studentClass.getName());

        loadStudentClass();
    }
    private void loadStudentClass(){
        //add each student as a clickable label in studentsPane
        ObservableList<Node> children = studentsPane.getChildren();
        studentClass.getStudents().forEach(student -> {
            StudentLabel label = new StudentLabel(student);
            label.setWrapText(true);
            label.setTextAlignment(TextAlignment.CENTER);

            children.add(label);
            plan.addStudentLabel(label);

            label.setOnMouseClicked(event -> {
                if(label.isInUse()){
                    if(event.getButton() != MouseButton.SECONDARY) return;
                    //Remove label from grid and re-add it to student pane
                    grid.getChildren().remove(label);
                    children.add(label);
                    Utils.resetDraggable(label);
                    label.setInUse(false);
                }
                else {
                    //add label to main pane
                    children.remove(label);
                    grid.getChildren().add(label);
                    Utils.makeDraggable(label);
                    label.setInUse(true);
                }});
        });
        //This sets the label position from file
        plan.load();
        plan.getStudentLabels().forEach(label -> {
            if(label.isInUse()){
                children.remove(label);
                grid.getChildren().add(label);
                Utils.makeDraggable(label);
            }

        });
    }
}