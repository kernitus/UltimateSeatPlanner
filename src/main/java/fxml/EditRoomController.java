package fxml;

import data.Room;
import data.furniture.*;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Cursor;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.Pane;
import utils.Utils;

import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;

public class EditRoomController extends AbstractController implements Initializable {

    @FXML private Button save;
    @FXML private Pane grid;
    @FXML private ImageView gridImage;
    @FXML private FlowPane furniturePane;
    @FXML private TextField name;

    private Room room;

    private static EditRoomController INSTANCE;

    public static EditRoomController getInstance() {
        return INSTANCE;
    }

    public void handleBackClick(MouseEvent event){
        save();//Save on back just in case
        super.handleBackClick(event);
    }
    public void handleHomeClick(MouseEvent event){
        save();
        super.handleHomeClick(event);
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        setupGrid();
        setupFurniture();
        INSTANCE = this;
    }

    /**
     * Setup grid image to scale with parent grid Pane
     */
    private void setupGrid(){
        gridImage.setFitWidth(grid.getWidth());
        gridImage.setFitHeight(grid.getHeight());
        gridImage.fitWidthProperty().bind(grid.widthProperty());
        gridImage.fitHeightProperty().bind(grid.heightProperty());
    }

    /**
     * Set mouse cursor to hand
     */
    private void setCursorToHand(MouseEvent event){
        ((Node) event.getTarget()).setCursor(Cursor.HAND);
    }

    //Table-related methods
    private void spawnVTable(){
        setupNewTable(new VTable());
    }
    private void spawnHTable(){
        setupNewTable(new HTable());
    }
    private void spawnRoundTable(){
        setupNewTable(new RoundTable());
    }
    private void spawnUpSemicircularTable(){
        setupNewTable(new UpSemicircularTable());
    }
    private void spawnDownSemicircularTable(){
        setupNewTable(new DownSemicircularTable());
    }
    private void setupNewTable(Table table){
        grid.getChildren().add(table);
        table.setupSeats(true);
        Utils.makeDraggable(table);
        makeDeleteable(table);

        room.addTable(table);
    }
    private void makeDeleteable(Table table){
        table.setOnMouseClicked(event -> {
            if(event.getButton() == MouseButton.SECONDARY)
                removeTable(table);
        });
    }
    private void removeTable(Table table){
        room.removeTable(table);
        grid.getChildren().remove(table);
    }

    /**
     * Setup the tables that spawn the draggable tables
     */
    private void setupFurniture(){
        Table[] tables = {new VTable(), new HTable(), new RoundTable(),
                new UpSemicircularTable(), new DownSemicircularTable()};
        for(Table table : tables)
            setupSpawningTable(table);
    }

    /**
     * Setup tables that spawn the draggable tables
     */
    private void setupSpawningTable(Table table){
        furniturePane.getChildren().add(table);
        table.setOnMouseEntered(this::setCursorToHand);

        EventHandler<MouseEvent> tableLambda;

        if(table instanceof VTable)
            tableLambda = event -> spawnVTable();
        else if(table instanceof HTable)
            tableLambda = event -> spawnHTable();
        else if(table instanceof RoundTable)
            tableLambda = event -> spawnRoundTable();
        else if(table instanceof UpSemicircularTable)
            tableLambda = event -> spawnUpSemicircularTable();
        else if(table instanceof DownSemicircularTable)
            tableLambda = event -> spawnDownSemicircularTable();
        else tableLambda = (event) -> System.out.println("Table type not recognised");

        table.setOnMouseClicked(tableLambda);
    }

    /**
     * Save the room layout to file
     */
    public void save(){
        room.save();
        String newName = name.getText();
        if(!newName.equals(room.getName())){//User changed room name
            room.rename(newName);
        }
    }
    /**
     * Load the room layout from file
     */
    public void load(Room room){
        this.room = room;
        load();
    }
    private void load(){
        List<Table> tables = room.getTables();
        Table[] tablesArray = tables.toArray(new Table[tables.size()]);
        tables.clear();
        for (Table table : tablesArray)
            setupNewTable(table);//This also re-adds the tables to the tables list

        //Fill in name textbox
        name.setText(room.getName());
    }
    /**
     * Deletes room
     */
    public void handleDeleteClick(MouseEvent event){
        if(!Utils.deleteAlert()) return;
        room.delete();
        Utils.goBack(this, ((Node) event.getSource()));
    }
}