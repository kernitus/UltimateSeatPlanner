package fxml;

import data.*;
import javafx.beans.property.ReadOnlyBooleanWrapper;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Insets;
import javafx.scene.control.*;
import javafx.scene.control.cell.CheckBoxTableCell;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;
import utils.Utils;

import java.io.File;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Optional;
import java.util.ResourceBundle;

public class PlansController extends AbstractController implements Initializable {

    private final ObservableList<Plan> data = FXCollections.observableArrayList();

    @FXML private Button newPlan;
    @FXML private TableView<Plan> plansTable;
    @FXML private TableColumn<Data, String> name;
    @FXML private TableColumn<Data, String> lastEdited;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        setupTable();
    }

    /**
     * Ask user for class and room to make plan for
     */
    public void handleNewPlanClick(MouseEvent event){
        Dialog<ButtonType> dialog = new Dialog<>();
        dialog.setTitle("Enter details");
        dialog.setHeaderText("Please choose room, class and name for plan");
        dialog.initOwner(Maine.getINSTANCE().getWindow());

        ButtonType customType = new ButtonType("Generate Plan", ButtonBar.ButtonData.NEXT_FORWARD);

        dialog.getDialogPane().getButtonTypes().addAll(customType,ButtonType.APPLY, ButtonType.CANCEL);

        GridPane grid = new GridPane();
        grid.setHgap(10);
        grid.setVgap(10);
        grid.setPadding(new Insets(20, 150, 10, 10));

        ComboBox<Room> room = new ComboBox<>();
        ComboBox<StudentClass> sclass = new ComboBox<>();

        TextField planName = new TextField();
        planName.setPromptText("Plan name");

        grid.add(new Label("Room:"), 0, 0);
        grid.add(room, 1, 0);
        grid.add(new Label("Class:"), 0, 1);
        grid.add(sclass, 1, 1);
        grid.add(new Label("Name:"), 0, 2);
        grid.add(planName, 1, 2);

        //Load all rooms into combobox
        ObservableList<Room> rooms = FXCollections.observableArrayList();
        rooms.addAll(RoomsController.getRooms());
        room.setItems(rooms);

        //Load all classes into combobox
        ObservableList<StudentClass> classes = FXCollections.observableArrayList();
        classes.addAll(ClassesController.getClasses());
        sclass.setItems(classes);

        dialog.getDialogPane().setContent(grid);

        //Get result
        Optional<ButtonType> result = dialog.showAndWait();
        result.ifPresent(buttonType -> {
            if(buttonType == ButtonType.CANCEL) return;

            Room raum = room.getSelectionModel().getSelectedItem();
            StudentClass studClass = sclass.getSelectionModel().getSelectedItem();
            String plan = planName.getText();

            if (raum == null || studClass == null || plan == null || plan.isEmpty()) return;
            raum.load();
            studClass.load();

            if(buttonType == ButtonType.APPLY)
                gotoEditNewPlan(newPlan, raum, studClass, plan);
            else if(buttonType == customType)//Generate plan
                generatePlanDialog(dialog,raum,studClass,plan);
        });
    }

    private void generatePlanDialog(Dialog previousDialog, Room room, StudentClass sclass, String planName){
        Dialog<ButtonType> dialog = new Dialog<>();
        dialog.setTitle("Choose parameters");
        dialog.setHeaderText("Please choose parameters for seating plan generation");
        dialog.initOwner(previousDialog.getOwner());
        //dialog.initOwner(Maine.getINSTANCE().getWindow());
        ButtonType exclusivePairs = new ButtonType("Exclusive Pairs",ButtonBar.ButtonData.NEXT_FORWARD);
        dialog.getDialogPane().getButtonTypes().addAll(exclusivePairs,ButtonType.APPLY, ButtonType.CANCEL);

        GridPane grid = new GridPane();
        dialog.getDialogPane().setContent(grid);

        Label lsex = new Label("Distribute by sex:");
        CheckBox sex = new CheckBox();
        Label lEAL = new Label("Distribute by EAL:");
        CheckBox EAL = new CheckBox();
        Label lSEN = new Label("Distribute by SEN:");
        CheckBox SEN = new CheckBox();
        Label lGT = new Label("Distribute by GT:");
        CheckBox GT = new CheckBox();

        grid.add(lsex,0,0);
        grid.add(sex,1,0);
        grid.add(lEAL,0,1);
        grid.add(EAL,1,1);
        grid.add(lSEN,0,2);
        grid.add(SEN,1,2);
        grid.add(lGT,0,3);
        grid.add(GT,1,3);

        Optional<ButtonType> btype = dialog.showAndWait();

        if(!btype.isPresent()) return;

        ButtonType resultBT = btype.get();

        if(resultBT == ButtonType.CANCEL) return;

        boolean bySex = sex.isSelected();
        boolean byEAL = EAL.isSelected();
        boolean bySEN = SEN.isSelected();
        boolean byGT = GT.isSelected();

        if(resultBT == exclusivePairs)
            exclusivePairsDialog(dialog,room,sclass,planName,bySex,byEAL,bySEN,byGT);
        else
            System.out.println("generatePlan()");
        //todo generatePlan();

    }

    private void exclusivePairsDialog(Dialog previousDialog, Room room,
                                      StudentClass sclass, String planName, boolean bySex,
                                      boolean byEAL, boolean bySEN, boolean byGT){
        Dialog<ButtonType> dialog = new Dialog<>();
        dialog.setTitle("Choose exclusive pairs");
        dialog.setHeaderText("Please choose pairs of students to exclude from sitting together");
        dialog.initOwner(previousDialog.getOwner());
        dialog.getDialogPane().getButtonTypes().addAll(ButtonType.APPLY, ButtonType.CANCEL);

        GridPane gpane = new GridPane();
        gpane.setHgap(10);
        gpane.setVgap(10);
        dialog.getDialogPane().setContent(gpane);

        TableView<Student> studentTable = new TableView<>();
        studentTable.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
        TableColumn<Student, String> firstNameColumn = new TableColumn<>("Name");
        firstNameColumn.setCellValueFactory(new PropertyValueFactory<>("firstName"));
        studentTable.getColumns().clear();
        studentTable.getColumns().add(firstNameColumn);
        TableColumn<Student, String> surnameColumn = new TableColumn<>("Surname");
        surnameColumn.setCellValueFactory(new PropertyValueFactory<>("surname"));
        studentTable.getColumns().add(surnameColumn);
        ObservableList<Student> data = FXCollections.observableArrayList(sclass.getStudents());
        studentTable.setItems(data);

        //Second table with all students except one selected in left table
        TableView<Student> chosenTable = new TableView<>();
        chosenTable.setEditable(true);
        chosenTable.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
        TableColumn<Student, String> chosenFullNameColumn = new TableColumn<>();
        chosenFullNameColumn.setCellValueFactory(new PropertyValueFactory<>("humanReadableFullName"));
        chosenTable.getColumns().add(chosenFullNameColumn);

        TableColumn<Student, Boolean> checkBoxColumn = new TableColumn<>();
        checkBoxColumn.setCellFactory(CheckBoxTableCell.forTableColumn(checkBoxColumn));
        chosenTable.getColumns().add(checkBoxColumn);
        checkBoxColumn.setEditable(true);

        //HashMap of student which contains arraylist of incompatible students each
        HashMap<Student,ArrayList<Student>> xpairs = new HashMap<>();
        data.forEach(student -> xpairs.put(student, new ArrayList<>()));

        checkBoxColumn.setOnEditCommit(event -> {
            boolean isExcluded = event.getNewValue();
            Student student = event.getRowValue();
            ArrayList<Student> excluded = xpairs.get(studentTable.getSelectionModel().getSelectedItem());
            if(isExcluded) excluded.add(student);
            else excluded.remove(student);
        });

        ObservableList<Student> chosenData = FXCollections.observableArrayList();
        chosenTable.setItems(chosenData);

        //VBox classList = new VBox();
        gpane.add(studentTable,0,0);
        gpane.add(chosenTable,1,0);
        //gpane.add(classList,1,0);

        studentTable.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection)-> {
            //When selection changes, change second column to display class list excluding selected
            chosenData.clear();
            chosenData.addAll(data);
            chosenData.remove(newSelection);
            //Clear the checkboxes
            //checkBoxColumn.setCellFactory(CheckBoxTableCell.forTableColumn(checkBoxColumn));
            //Load up the already-checked checkboxes
            ArrayList<Student> excludedStudents = xpairs.get(newSelection);
            System.out.println("excluded students: " + xpairs.toString());
            //todo
            checkBoxColumn.setCellValueFactory(cellData -> new ReadOnlyBooleanWrapper(excludedStudents.contains(cellData.getValue())));
        });

        Optional<ButtonType> result = dialog.showAndWait();
    }

    private Room getSelectedRoom(ComboBox<String> roomSelector){
        String sel = roomSelector.getSelectionModel().getSelectedItem();
        if(sel==null || sel.isEmpty()) return null;
        return new Room(sel);
    }
    private StudentClass getSelectedClass(ComboBox<String> classSelector){
        String sel = classSelector.getSelectionModel().getSelectedItem();
        if(sel == null || sel.isEmpty()) return null;
        return new StudentClass(sel);
    }

    private void gotoEditNewPlan(Button button, Room room, StudentClass sclass, String name){
        if(room==null || sclass==null || name==null || name.isEmpty()) return;
        Utils.goForward(this,button,"EditPlan.fxml");
        EditPlanController.getInstance().load(new Plan(room,sclass,name));
    }

    /**
     * Handle clicking on table cell
     */
    public void handleTableClick(MouseEvent event){
        TablePosition fcell = plansTable.getFocusModel().getFocusedCell();
        //Get plan from clicked cell
        int row = fcell.getRow();
        Plan plan = data.get(row);

        //Switch to edit plan view
        Utils.goForward(this,plansTable,"EditPlan.fxml");
        EditPlanController.getInstance().load(plan);//Load the selected plan
    }

    private void setupTable(){
        name.setCellValueFactory(new PropertyValueFactory<Data, String>("name"));
        lastEdited.setCellValueFactory(new PropertyValueFactory<Data, String>("lastEdited"));

        //populate the table asynchronously to reduce lag
        new Thread(this::populateTable).start();
    }

    private void populateTable(){
        File[] files = new File(Plan.getUSPDirectoryPath() + "plans" + File.separator).listFiles();
        plansTable.setItems(data);
        if((files != null ? files.length : 0) <= 0) return;
        for (File file : files) {
            String[] fileNameParts = file.getName().replaceAll(".usp", "").split("_");
            //Plan file name: roomName_className_planName.usp
            data.add(new Plan(
                    new Room(fileNameParts[0]),
                    new StudentClass(fileNameParts[1]),
                    fileNameParts[2]));
        }
    }
}
