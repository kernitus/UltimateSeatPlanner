package fxml;

import data.Data;
import data.Room;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TablePosition;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import utils.Utils;

import java.io.File;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;

public class RoomsController extends AbstractController implements Initializable {

    //@FXML private Button newRoom;
    private final ObservableList<Room> data = FXCollections.observableArrayList();

    @FXML private TableView<Room> roomsTable;
    @FXML private TableColumn<Data, String> name;
    @FXML private TableColumn<Data, String> lastEdited;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        setupTable();
    }

    /**
     * Add a new room to the table
     */
    public void handleNewRoomClick(MouseEvent event){
        String newName = Utils.inputDialog("Please enter room name:");
        data.add(new Room(newName));
    }

    /**
     * Handle clicking on table cell
     */
    public void handleTableClick(MouseEvent event){
        //Single click should open corresponding room
        TablePosition fcell = roomsTable.getFocusModel().getFocusedCell();
        //Get room from clicked cell
        int row = fcell.getRow();
        Room room = data.get(row);

        //Switch to edit room view
        Utils.goForward(this,roomsTable,"EditRoom.fxml");
        EditRoomController.getInstance().load(room);//Load the selected room
    }

    private void setupTable(){
        name.setCellValueFactory(new PropertyValueFactory<Data, String>("name"));
        lastEdited.setCellValueFactory(new PropertyValueFactory<Data, String>("lastEdited"));

        //populate the table asynchronously to reduce lag
        new Thread(this::populateTable).start();
    }

    private void populateTable(){
        roomsTable.setItems(data);
        data.addAll(getRooms());
    }

    public static ArrayList<Room> getRooms(){
        ArrayList<Room> rooms = new ArrayList<>();
        File[] files = new File(Room.getUSPDirectoryPath() + "rooms" + File.separator).listFiles();
        if((files != null ? files.length : 0) <= 0) return rooms;
        for (File file : files) {
            Room room = new Room(file.getName().replaceAll(".usp", ""));
            rooms.add(room);
        }
        return rooms;
    }
}
