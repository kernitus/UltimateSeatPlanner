package fxml;

import javafx.scene.Node;
import javafx.scene.input.MouseEvent;
import utils.Utils;

public abstract class AbstractController {


    public void handleBackClick(MouseEvent event){
        Utils.goBack(this,(Node) event.getSource());
    }
    public void handleHomeClick(MouseEvent event){
        Utils.goHome(this,(Node) event.getSource());
    }
}
