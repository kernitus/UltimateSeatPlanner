package fxml;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Screen;
import javafx.stage.Stage;
import javafx.stage.Window;

public class Maine extends Application {

    private Window owner;
    private static Maine INSTANCE;

    @Override
    public void start(Stage primaryStage) throws Exception{
        Parent root = FXMLLoader.load(getClass().getResource("/fxml/MainMenu.fxml"));
        primaryStage.setTitle("Ultimate Seat Planner");
        primaryStage.setMaximized(true);
        primaryStage.setScene(new Scene(root, Screen.getPrimary().getVisualBounds().getWidth(), Screen.getPrimary().getVisualBounds().getHeight()));
        primaryStage.show();

        INSTANCE = this;
        owner = primaryStage.getOwner();
    }

    public Window getWindow(){
        return owner;
    }

    public static Maine getINSTANCE(){
        return INSTANCE;
    }

    public static void main(String[] args) {
        launch(args);
    }
}