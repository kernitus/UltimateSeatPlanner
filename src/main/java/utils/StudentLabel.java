package utils;

import data.Student;
import javafx.scene.control.Label;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;

public class StudentLabel extends Label {

    private final Student student;
    private boolean isInUse = false;

    public StudentLabel(Student student){
        this.student = student;
        setFont(Font.font("Liberation Mono", FontWeight.BOLD, 14));
        setText(student.getHumanReadableFullName().replaceAll(" ","\n"));
    }

    public Student getStudent() {
        return student;
    }

    public void setInUse(boolean isInUse){
        this.isInUse = isInUse;
    }
    public boolean isInUse() {
        return isInUse;
    }

}
