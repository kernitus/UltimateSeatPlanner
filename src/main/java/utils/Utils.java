package utils;

import data.Location;
import javafx.fxml.FXMLLoader;
import javafx.scene.Cursor;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TextInputDialog;
import javafx.stage.Stage;

import java.io.IOException;
import java.util.Optional;
import java.util.Stack;

public class Utils {

    public static void goBack(Object controller, Node button){
        switchFXMLFile(controller, button, sceneHistoryGoBack());
    }
    public static void goForward(Object controller, Node button, String newScene){
        sceneHistoryGoForward(newScene);
        switchFXMLFile(controller, button, newScene);
    }
    public static void goHome(Object controller, Node button){
        String first = sceneHistory.firstElement();
        switchFXMLFile(controller, button, first);
        sceneHistory.clear();
        sceneHistoryGoForward(first);
    }

    private static void switchFXMLFile(Object controller, Node button, String file){
        try {
            Parent newPage = FXMLLoader.load(controller.getClass().getResource(file));
            Scene scene = button.getScene();
            scene.setRoot(newPage);
            ((Stage) scene.getWindow()).setMaximized(true);
        } catch (IOException e) {
            System.err.println("Error switching pages");
            e.printStackTrace();
        }
    }

    private static Stack<String> sceneHistory = new Stack<>();
    public static void sceneHistoryGoForward(String name){
        sceneHistory.push(name);
    }
    public static String sceneHistoryGoBack(){
        sceneHistory.pop(); //Pop current scene
        return sceneHistory.peek(); //Return previous scene
    }

    public static Location getParentRelativeLocation(Node node, Node parent) {
        double x = node.getLayoutX();
        double y = node.getLayoutY();

        for (Node owner = node.getParent(); !parent.equals(owner); owner = owner.getParent()) {
            x += owner.getLayoutX();
            y += owner.getLayoutY();
        }

        return new Location(x, y);
    }

    public static void makeDraggable(Node node) {
        class Change {
            double x, y;
        }

        final Change change = new Change();
        node.setOnMousePressed(mouseEvent -> {
            change.x = node.getLayoutX() - mouseEvent.getSceneX();
            change.y = node.getLayoutY() - mouseEvent.getSceneY();
            node.setCursor(Cursor.MOVE);
        });

        node.setOnMouseReleased(mouseEvent -> node.setCursor(Cursor.HAND));
        node.setOnMouseDragged(mouseEvent -> {
            double newX = mouseEvent.getSceneX() + change.x;
            double newY = mouseEvent.getSceneY() + change.y;
            node.setLayoutX(newX);
            node.setLayoutY(newY);
        });
        node.setOnMouseEntered(mouseEvent -> node.setCursor(Cursor.HAND));
    }
    public static void resetDraggable(Node node){
        node.setOnMousePressed(null);
        node.setOnMouseReleased(null);
        node.setOnMouseDragged(null);
        node.setOnMouseEntered(null);
    }

    /**
     * Alert user of imminent deletion
     * @return true to proceed with deletion
     */
    public static boolean deleteAlert(){
        Alert alert = new Alert(Alert.AlertType.WARNING);
        alert.setTitle("Delete Warning");
        alert.setHeaderText("Requesting deletion confirmation");
        alert.setContentText("Are you sure you want to delete?");

        alert.getButtonTypes().clear();
        alert.getButtonTypes().addAll(ButtonType.NO,ButtonType.YES);

        Optional<ButtonType> result = alert.showAndWait();
        if(result.get() == ButtonType.YES) return true;
        else return false;
    }

    public static String inputDialog(String promptText) {
        TextInputDialog dialog = new TextInputDialog();
        dialog.setTitle("Input Dialog");
        dialog.setHeaderText("Please Input");
        dialog.setContentText(promptText);

        Optional<String> result = dialog.showAndWait();
        return result.orElse("");
    }
}
