# Ultimate Seat Planner

A simple JavaFX program that takes class lists and classroom layouts, and allows making and visualising student seating plans, based on any criteria (e.g. age, skill, sex).

## Home
The home page provides large buttons with the various screens the user can go to.

![Home](screenshots/home.png)

## Classes
Any of the three main buttons lead to a table where a list of the existing rooms, classes and plans can be viewed, and new ones created.

![Classes](screenshots/classes.png)

![Rooms](screenshots/rooms.png)


## Adding a room
A new room may be added by simply pressing the *New Room* button and specifying a name in the dialog.

![Room Dialog](screenshots/room_dialog.png)

## Room
A room can be edited by dragging and dropping the furniture onto the grid, and then clicking on the red squares to turn them green, signifying a seat is available.

![Room](screenshots/room.png)

## Students
Students can be added to a class by pressing the *Add student* button and filling in their details.

![Students](screenshots/students.png)

## Create plan
A seating plan can be creating by selecting an existing room and class from the dropdowns.

![Students](screenshots/plan_dialog.png)

## Plan
Finally, the plan can be viewed and edited by assigning the students' names to the available seats from the panel on the right.

![Students](screenshots/plan.png)

-------

## Running the project
A Gradle file is provided with the project. Simply running `gradle run` will fetch the required dependencies, compile the project and launch it.
